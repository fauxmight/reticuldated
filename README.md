# reticuldated

reticuldated is a python-based curses caldav client
-----------------------------------------------------------------------

This project takes inspiration from
[remind](https://www.roaringpenguin.com/products/remind), and from
[khal](https://lostpackets.de/khal/). The reasons this exists and I
don't just use those are:

 - caldav: remind doesn't play nicely with caldav, and I want my console application to use the same data as everything else
 - avoiding vdirsyncer: I want to directly work with the caldav server's contents. If I wanted to work on locally stored data, I wouldn't use dav.

Basic Functionality Attained!
-----------------------------
reticuldated does what I intended it to do as of this commit.
It could still use better error checking. I have not tested
it on servers that are not DAViCal or Baïkal, and I don't have
many test users who are not me. (It could also do with some cleanup.
The display month function is just hideously huge, and I don't
currently have a display_week, display_day, or display_multiweek
function because I never really would use them myself, but this
*works* just as it is. If you've tried it and like/hate/need fixes for
it, I would love to hear from you.

The project to depends upon
[python caldav](https://github.com/python-caldav/caldav),
[calendar](https://docs.python.org/3.5/library/calendar.html), and
[datetime](https://docs.python.org/3.5/library/datetime.html).

At present, the only display possible is "month." "Week," and "day" views are planned.

Regarding [Baïkal](http://sabre.io/baikal/)
-------------------------------------------

As of Baïkal version 0.4.6, Baïkal does NOT play nicely with the python caldav library.
Specifically, the date_search function, when used with two date arguments (a beginning date
and an end date) causes an internal server error with Baïkal. I am not going to invest the
time in determining why and have personally switched to [DAViCal](https://www.davical.org/)
instead.
